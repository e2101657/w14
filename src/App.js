import React, { useState } from 'react';
import './App.css';

function App() {
  const [chatInput, setChatInput] = useState('');
  const sendChat = () => {
    const chats = JSON.parse(localStorage.getItem('chats')) || [];
    chats.push(chatInput);
    localStorage.setItem('chats', JSON.stringify(chats));
    setChatInput('');
  };
  const showMessages = () => {
    const chats = JSON.parse(localStorage.getItem('chats')) || [];
    console.log('Stored Chats:', chats);
  };

  return (
    <div className="App">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.
      </p>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ultrices in iaculis nunc sed augue lacus viverra. Eu augue ut lectus arcu. Urna condimentum mattis pellentesque id nibh tortor. Tempus iaculis urna id volutpat. Vestibulum morbi blandit cursus risus at ultrices mi tempus. Ultricies leo integer malesuada nunc vel risus commodo viverra maecenas. In mollis nunc sed id semper risus in. Sed turpis tincidunt id aliquet. Nec tincidunt praesent semper feugiat nibh sed. Mattis rhoncus urna neque viverra justo nec ultrices dui. In aliquam sem fringilla ut morbi tincidunt augue interdum velit.
      </p>
      <div className="chatbox">
        <input
        type="text"
        id="chat"
        value={chatInput}
        onChange={(e) => setChatInput(e.target.value)}
/>
      <button onClick={sendChat}>Send</button>
      <button onClick={showMessages}>Show messages</button>
    </div>
    </div>
  );}
export default App;



